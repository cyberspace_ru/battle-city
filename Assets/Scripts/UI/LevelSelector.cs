﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSelector : MonoBehaviour
{
    public Texture2D Texture;

    public void SelectMap()
    {
        GameController.Instance.CurrentMap = Texture;
        GameController.Instance.LaunchGame();
    }

}
