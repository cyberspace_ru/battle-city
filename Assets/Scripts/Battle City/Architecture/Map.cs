﻿using System;
using System.Linq;
using System.Collections.Generic;
using BattleCity.Architecture.Enums;
using BattleCity.Architecture.Factories;
using BattleCity.Architecture.CollisionSystem;

namespace BattleCity.Architecture
{
    public class Map
    {
        public Size Size { get; private set; }
        public List<GameObject> Instances { get; private set; }
        public Collider Bounds { get; private set; }
        public event Action<GameObject> OnCreateInstance;

        private GameObjectFactory gameObjecFactory = new GameObjectFactory();

        public Map(Size size, List<GameObject> instances)
        {
            this.Size = size;
            this.Instances = instances;
            this.Bounds = new Collider(new Point(0, 0), Size);
        }

        private void CallGlobalAction(Action<GameObject> action)
        {
            int count = Instances.Count;
            for (int i = 0; i < Instances.Count; i++)
            {
                action(Instances[i]);
                if (count != Instances.Count)
                {
                    i += Instances.Count - count;
                    count = Instances.Count;
                }
            }
        }

        public void Start()
        {
            CallGlobalAction(go => go.Start(this));
        }

        public void Update()
        {
            CallGlobalAction(go => go.ForceUpdate(this));
        }

        public void CheckTriggers()
        {
            CallGlobalAction(go => CheckTrigger(go));
        }

        private void CheckTrigger(GameObject go)
        {
            if (!OnBounds(go.Collider))
            {
                go.OnExitBounds(this);
            }
            else if (go.Collider.IsTrigger)
            {
                GetCollision(go).ForEach(x =>
                {
                    x.OnCollision(this, go);
                    go.OnTrigger(this, x);
                });
            }
        }

        public bool RegisterObject(GameObject go)
        {
            if (go != null)
            {
                if (!go.Collider.IsTrigger)
                    throw new Exception("Can't add solid object."); // Логика игры не предполагает добавление не триггерных объектов. Иначе, необходима лишняя проверка на коллизию перед добавлением.
                if (OnBounds(go.Collider))
                {
                    go.Start(this);
                    OnCreateInstance.ToMaybe().Do(x => x(go));
                    Instances.Add(go);
                    CheckTrigger(go);
                    return true;
                }
                else
                {
                    go.Destroy(this);
                }
            }
            return false;
        }

        public void CreateBonus()
        {
            GameObject bonus = gameObjecFactory.Produce(new Configurations.GameObjectConfiguration
            {
                Point = Random.RandomPoint(Size),
                Type = GameObjectType.Bonus
            });
            RegisterObject(bonus);
        }

        public bool OnBounds(Collider collider)
        {
            return Bounds.Contains(collider);
        }

        public List<GameObject> GetCollision(GameObject ignored, bool ignoreTriggers = true)
        {
            return GetCollision(ignored.Collider, ignored, ignoreTriggers);
        }

        public List<GameObject> GetCollision(Collider collider, GameObject ignored, GameObjectType exlude, bool ignoreTriggers = true)
        {
            Predicate<GameObject> predicate = (go) =>
                go != ignored
                && exlude != go.Type
                && (!go.Collider.IsTrigger || !ignoreTriggers) // Либо коллайдер не является триггером, либо мы не игнорируем триггеры.
                && collider.Overlaps(go.Collider);
            return Instances.FindAll(predicate);
        }

        public List<GameObject> GetCollision(Collider collider, GameObject ignored, bool ignoreTriggers = true)
        {
            Predicate<GameObject> predicate = (go) =>
                go != ignored && (!go.Collider.IsTrigger || !ignoreTriggers) && collider.Overlaps(go.Collider);
            return Instances.FindAll(predicate);
        }

        public bool CanBePlaced(Collider collider, GameObject ignored)
        {
            if (!OnBounds(collider))
                return false;
            return GetCollision(collider, ignored).Count == 0;
        }

    }
}
