﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleCity.Architecture.Agents
{
    public abstract class Agent
    {
        protected GameObject driven;
        protected Command command;

        public Agent(GameObject driven)
        {
            this.driven = driven;
        }

        public void ExecuteCommand()
        {
            if (command != null)
                command.Execute();
            command = null;
        }

    }
}
