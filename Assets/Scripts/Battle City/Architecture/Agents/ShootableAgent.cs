﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BattleCity.Architecture;
using BattleCity.Architecture.Commands;

namespace BattleCity.Architecture.Agents
{
    public class ShootableAgent : Agent
    {
        private Map map;

        public ShootableAgent(Map map, Player driven) : base(driven)
        {
            this.map = map;
        }

        public void Shoot()
        {
            command = new ShootCommand(map, (Player) driven);
        }

    }
}
