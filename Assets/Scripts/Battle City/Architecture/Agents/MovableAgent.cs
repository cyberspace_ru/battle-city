﻿using System;
using System.Collections.Generic;
using System.Linq;
using BattleCity.Architecture.Commands;

namespace BattleCity.Architecture.Agents
{
    public class MovableAgent : Agent
    {
        private Map map;

        public MovableAgent(Map map, GameObject driven) : base(driven)
        {
            this.map = map;
        }

        public void MoveToUp()
        {
            command = new MoveCommand(map, driven as IMoveable, Enums.MoveType.Up);
        }

        public void MoveToRight()
        {
            command = new MoveCommand(map, driven as IMoveable, Enums.MoveType.Right);
        }

        public void MoveToDown()
        {
            command = new MoveCommand(map, driven as IMoveable, Enums.MoveType.Down);
        }

        public void MoveToLeft()
        {
            command = new MoveCommand(map, driven as IMoveable, Enums.MoveType.Left);
        }
    }
}
