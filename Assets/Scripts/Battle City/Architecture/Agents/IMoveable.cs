﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleCity.Architecture.Agents
{
    public interface IMoveable
    {
        void Move(Map map, Enums.MoveType direction);
    }
}
