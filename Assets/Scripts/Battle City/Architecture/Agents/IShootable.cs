﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleCity.Architecture.Agents
{
    public interface IShootable
    {
        void Shoot(Map map);
    }
}
