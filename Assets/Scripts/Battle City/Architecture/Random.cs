﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleCity.Architecture
{
    static class Random
    {

        public static int Next(int i)
        {
            return new System.Random(Guid.NewGuid().GetHashCode()).Next(i);
        }

        public static double NextDouble()
        {
            return new System.Random(Guid.NewGuid().GetHashCode()).NextDouble();
        }

        public static Point RandomPoint(Size size)
        {
            return new Point(Next(size.Width), Next(size.Height));
        }

    }
}
