﻿using System;
using System.Collections.Generic;
using BattleCity.Architecture.Enums;
using BattleCity.Architecture.Configurations;
using BattleCity.Architecture.CollisionSystem;

namespace BattleCity.Architecture.Factories
{
    class MissileFactory : GameObjectFactory
    {

        public GameObject Produce(GameObject initiator)
        {
            Point point = new Point();
            Size size;
            MoveType direction = initiator.Direction;
            Collider collider = initiator.Collider;
            OrientationType spreadType = direction.ToOrientationType();
            switch (spreadType)
            {
                case OrientationType.Vertical:
                    size = new Size(collider.Size.Width, 1);
                    point.X = (int)Math.Floor(collider.Center.X - (size.Width / 2));
                    switch (direction)
                    {
                        case MoveType.Up:
                            point.Y = collider.YMin - size.Height;
                            break;
                        default:
                            point.Y = collider.YMax + (size.Height - 1);
                            break;
                    }
                    break;
                default:
                    size = new Size(1, collider.Size.Height);
                    point.Y = (int)Math.Floor(collider.Center.Y - (size.Height / 2));
                    switch (direction)
                    {
                        case MoveType.Right:
                            point.X = collider.XMax + (size.Width - 1);
                            break;
                        default:
                            point.X = collider.XMin - size.Width;
                            break;
                    }
                    break;
            }
            return Produce(new MissileConfiguration
            {
                Owner = initiator,
                Type = GameObjectType.Missile,
                Direction = direction,
                Point = point,
                Size = size
            });
        }

    }
}
