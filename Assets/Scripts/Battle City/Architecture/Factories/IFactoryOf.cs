﻿using System.Collections;
using System.Collections.Generic;
using BattleCity.Architecture.Configurations;

namespace BattleCity.Architecture
{
    public interface IFactoryOf<ProductType, ConfigurationType> 
        where ConfigurationType : IConfigurationOf<ProductType>
    {
        ProductType Produce(ConfigurationType config);
    }
}
