﻿using System;
using System.Collections.Generic;
using BattleCity.Architecture.Enums;
using BattleCity.Architecture.Configurations;
using BattleCity.Architecture.CollisionSystem;

namespace BattleCity.Architecture.Factories
{
    public class GameObjectFactory : IFactoryOf<GameObject, GameObjectConfiguration>
    {
        public GameObject Produce(GameObjectConfiguration config)
        {
            switch (config.Type)
            {
                case GameObjectType.Player:
                    return new Player
                    {
                        UpdateRate = 3,
                        Type = GameObjectType.Player,
                        Collider = new Collider(config.Point, new Size(4, 4))
                    };
                case GameObjectType.DoubleTower:
                    return new Tower
                    {
                        Type = GameObjectType.DoubleTower,
                        Health = 4,
                        Direction = MoveTypeExtension.Random(),
                        Collider = new Collider(config.Point, new Size(2, 2))
                    };
                case GameObjectType.QuadTower:
                    return new Tower
                    {
                        Type = GameObjectType.QuadTower,
                        Health = 6,
                        Direction = MoveTypeExtension.Random(),
                        Collider = new Collider(config.Point, new Size(4, 4))
                    };
                case GameObjectType.Missile:
                    MissileConfiguration missile = (MissileConfiguration)config;
                    return new Missile
                    {
                        Owner = missile.Owner,
                        Type = GameObjectType.Missile,
                        Direction = config.Direction,
                        Collider = new Collider(config.Point, config.Size, true)
                    };
                case GameObjectType.Brick:
                    return new SolidBarrier
                    {
                        Type = GameObjectType.Brick,
                        Collider = new Collider(config.Point, new Size(1, 1))
                    };
                case GameObjectType.Concrete:
                    return new SolidBarrier
                    {
                        IgnoreDamage = true,
                        Type = GameObjectType.Concrete,
                        Collider = new Collider(config.Point, new Size(1, 1))
                    };
                case GameObjectType.Water:
                    return new LiquidBarrier
                    {
                        Type = GameObjectType.Water,
                        Collider = new Collider(config.Point, new Size(4, 4))
                    };
                case GameObjectType.Bonus:
                    return new Bonus
                    {
                        Type = GameObjectType.Water,
                        BonusType = BonusTypeExtension.Random(),
                        Collider = new Collider(config.Point, new Size(4, 4), true)
                    };
            }
            return null;
        }
    }
}
