﻿using System;
using System.Linq;
using BattleCity.Architecture.Configurations;
using BattleCity.Architecture.Initializers;
using BattleCity.Architecture.Factories;

namespace BattleCity.Architecture
{
    public class Core
    {
        public Map Map { get; private set; }
        public bool Finished { get; private set; }
        public event Action OnWin;
        public event Action OnDefeat;

        public void Launch(MapInitializer initializer)
        {
            Map = InitializeMap(initializer);
            Map.Start();
        }

        public Map InitializeMap(MapInitializer initializer)
        {
            return initializer.Initialize();
        }

        public void ForceUpdate()
        {
            if (!Finished)
            {
                if (Random.NextDouble() > 0.997)
                    Map.CreateBonus();
                Map.Update();
                Map.CheckTriggers();
                if (Map.Instances.Find(x => x is Player) == null)
                {
                    Finished = true;
                    OnDefeat.ToMaybe().Do(x => x());
                }
                if (Map.Instances.Find(x => x is Tower) == null)
                {
                    Finished = true;
                    OnWin.ToMaybe().Do(x => x());
                }
            }
        }
    }
}
