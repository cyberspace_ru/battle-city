﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleCity.Architecture.Enums
{
    public enum BonusType
    {
        Attack,
        Clock,
        Defence
    }

    public static class BonusTypeExtension
    {
        public static BonusType Random()
        {
            Array values = Enum.GetValues(typeof(BonusType));
            return (BonusType)values.GetValue(Architecture.Random.Next(values.Length));
        }
    }
}
