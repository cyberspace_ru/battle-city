﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace BattleCity.Architecture.Enums
{
    public enum MoveType
    {
        Up,
        Right,
        Down,
        Left
    }

    public static class MoveTypeExtension
    {
        public static OrientationType ToOrientationType(this MoveType type)
        {
            switch (type)
            {
                case MoveType.Up:
                case MoveType.Down:
                    return OrientationType.Vertical;
                default:
                    return OrientationType.Horizontal;
            }
        }

        public static List<MoveType> AsList()
        {
            return Enum.GetValues(typeof(MoveType)).Cast<MoveType>().ToList();
        }

        public static MoveType Random()
        {
            Array values = Enum.GetValues(typeof(MoveType));
            return (MoveType)values.GetValue(Architecture.Random.Next(values.Length));
        }
    }
}
