﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleCity.Architecture.Enums
{
    public enum OrientationType
    {
        Vertical,
        Horizontal
    }
}
