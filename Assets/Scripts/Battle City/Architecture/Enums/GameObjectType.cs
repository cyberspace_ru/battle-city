﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleCity.Architecture.Enums
{
    public enum GameObjectType
    {
        Undefined,
        Player,
        QuadTower,
        DoubleTower,
        Missile,
        Brick,
        Concrete,
        Water,
        Bonus
    }
}
