﻿using System;
using System.Collections.Generic;
using BattleCity.Architecture.Enums;
using BattleCity.Architecture;

namespace BattleCity.Architecture.CollisionSystem
{
    class RayCast
    {
        public static List<GameObject> CastBySolidObjects(Map map, GameObject source, MoveType direction, int steps)
        {
            Collider collider = source.Collider;
            while (map.OnBounds(collider) || steps > 0)
            {
                steps -= 1;
                collider = collider.Shift(direction);
                List<GameObject> collisions = map.GetCollision(collider, source, GameObjectType.Water);
                if (collisions.Count != 0)
                    return collisions;
            }
            return null;
        }

    }
}
