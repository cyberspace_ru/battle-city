﻿using System;
using System.Collections.Generic;
using BattleCity.Architecture.Enums;

namespace BattleCity.Architecture.CollisionSystem
{
    public struct Collider
    {
        private int x;
        private int y;
        private int width;
        private int height;
        public bool IsTrigger { get; set; }

        public static Collider NotExist { get { return new Collider(new Point(-1, -1), -1); } }

        public Size Size { get { return new Size(width, height); } }
        public Point Point { get { return new Point(x, y); } }
        public int XMin { get { return x; } }
        public int YMin { get { return y; } }
        public int XMax { get { return x + width; } }
        public int YMax { get { return y + height; } }
        public PointF Center { get { return new PointF(XMin + width / 2.0f, YMin + height / 2.0f); } }

        public Collider(Point point, int size, bool isTrigger = false)
        {
            this.x = point.X;
            this.y = point.Y;
            this.width = size;
            this.height = size;
            this.IsTrigger = isTrigger;
        }

        public Collider(Point point, Size size, bool isTrigger = false)
        {
            this.x = point.X;
            this.y = point.Y;
            this.width = size.Width;
            this.height = size.Height;
            this.IsTrigger = isTrigger;
        }

        public bool Overlaps(Collider other)
        {
            return other.XMax > this.XMin && other.XMin < this.XMax && other.YMax > this.YMin && other.YMin < this.YMax;
        }

        public bool Contains(Collider other)
        {
            return (this.XMin <= other.XMin) && (other.XMax <= this.XMax) &&
                   (this.YMin <= other.YMin) && (other.YMax <= this.YMax);
        }

        public override int GetHashCode()
        {
            return (int)((UInt32)x ^
                        (((UInt32)y << 13) | ((UInt32)y >> 19)) ^
                        (((UInt32)width << 26) | ((UInt32)width >> 6)) ^
                        (((UInt32)height << 7) | ((UInt32)height >> 25)));
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Collider))
                return false;
            Collider another = (Collider)obj;
            return this.x == another.x 
                && this.y == another.y 
                && this.width == another.width 
                && this.height == another.height;
        }

        public Collider Shift(MoveType direction)
        {
            Point pPoint = Point;
            Point nPoint = ShiftPointByDirection(pPoint, direction);
            return CopyBySpecificPoint(nPoint);
        }

        private static Point ShiftPointByDirection(Point point, MoveType direction)
        {
            switch (direction)
            {
                case MoveType.Up:
                    return new Point(point.X, point.Y - 1);
                case MoveType.Right:
                    return new Point(point.X + 1, point.Y);
                case MoveType.Down:
                    return new Point(point.X, point.Y + 1);
                default:
                    return new Point(point.X - 1, point.Y);
            }
        }

        public Collider Copy()
        {
            return new Collider(Point, Size, IsTrigger);
        }

        public Collider CopyBySpecificPoint(Point point)
        {
            return new Collider(point, Size, IsTrigger);
        }

    }
}
