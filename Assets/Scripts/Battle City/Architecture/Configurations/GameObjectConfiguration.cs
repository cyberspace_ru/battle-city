﻿using System;
using System.Collections.Generic;
using BattleCity.Architecture.Enums;

namespace BattleCity.Architecture.Configurations
{
    public class GameObjectConfiguration : IConfigurationOf<GameObject>
    {
        public GameObjectType Type;
        public MoveType Direction;
        public Point Point;
        public Size Size;
    }
}
