﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleCity.Architecture
{
    public class Timer : IUpdatable
    {
        public int Delay { get; set; }
        public bool HasElapsed { get; private set; }

        private int currentDelay;

        public Timer(int delay)
        {
            this.Delay = delay;
        }

        public void Reset()
        {
            this.currentDelay = Delay;
            this.HasElapsed = false;
        }

        public void BeforeUpdate()
        {
            if (this.currentDelay-- <= 0 && !HasElapsed)
                HasElapsed = true;
        }

        public void OnUpdate()
        {
        }

        public void AfterUpdate()
        {
        }
    }
}
