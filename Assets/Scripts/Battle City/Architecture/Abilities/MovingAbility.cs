﻿using System;
using System.Collections.Generic;
using BattleCity.Architecture.Enums;
using BattleCity.Architecture.CollisionSystem;

namespace BattleCity.Architecture.Abilities
{
    public class MovingAbility : Ability<DynamicGameObject>
    {
        public MoveType MoveDirection;

        public MovingAbility(int cooldown) : base(cooldown) { }

        protected override void Action(Map map, DynamicGameObject self)
        {
            Collider nCollider = self.Collider.Shift(MoveDirection);
            self.ChangeDirection(MoveDirection);
            if (nCollider.IsTrigger || map.CanBePlaced(nCollider, self))
                self.ChangePoint(nCollider);
        }
    }
}
