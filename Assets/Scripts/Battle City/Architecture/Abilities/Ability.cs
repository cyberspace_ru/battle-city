﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleCity.Architecture.Abilities
{
    public abstract class Ability<T> : Timer where T : GameObject
    {
        public Ability(int cooldown) : base(cooldown) { }

        abstract protected void Action(Map map, T self);

        public bool Use(Map map, T self)
        {
            if (base.HasElapsed)
            {
                Action(map, self);
                base.Reset();
                return true;
            }
            return false;
        }
    }
}
