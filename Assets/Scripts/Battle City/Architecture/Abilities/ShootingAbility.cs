﻿using System;
using System.Collections.Generic;
using BattleCity.Architecture.Factories;

namespace BattleCity.Architecture.Abilities
{
    public class ShootingAbility : Ability<GameObject>
    {
        private static MissileFactory missileFactory = new MissileFactory();

        public ShootingAbility(int delay) : base(delay) { }

        protected override void Action(Map map, GameObject self)
        {
            GameObject missile = missileFactory.Produce(self);
            map.RegisterObject(missile);
        }
    }
}
