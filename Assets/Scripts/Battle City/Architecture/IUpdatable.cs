﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleCity.Architecture
{
    public interface IUpdatable
    {
        /// <summary>
        /// Выполняеся КАЖДЫЙ ход до <seealso cref="GameObject.Update(Map)"/>.
        /// </summary>
        void BeforeUpdate();

        /// <summary>
        /// Выполняеся только во время обновления <seealso cref="GameObject"/>.
        /// </summary>
        void OnUpdate();


        /// <summary>
        /// Выполняеся КАЖДЫЙ ход после <seealso cref="GameObject.Update(Map)"/>.
        /// </summary>
        void AfterUpdate();
    }
}
