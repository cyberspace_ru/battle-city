﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BattleCity.Architecture.Agents;

namespace BattleCity.Architecture.Commands
{
    public class ShootCommand : Command
    {
        public Map Map { get; private set; }
        public IShootable GameObject { get; private set; }

        public ShootCommand(Map map, IShootable gameObject)
        {
            this.Map = map;
            this.GameObject = gameObject;
        }

        public override void Execute()
        {
            GameObject.Shoot(Map);
        }
    }
}
