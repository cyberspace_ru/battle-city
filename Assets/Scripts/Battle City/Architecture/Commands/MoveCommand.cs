﻿using System;
using System.Collections.Generic;
using BattleCity.Architecture.Agents;
using BattleCity.Architecture.Enums;

namespace BattleCity.Architecture.Commands
{
    public class MoveCommand : Command
    {
        public Map Map { get; private set; }
        public IMoveable GameObject { get; private set; }
        public MoveType Direction { get; private set; }

        public MoveCommand(Map map, IMoveable gameObject, MoveType direction)
        {
            this.Map = map;
            this.GameObject = gameObject;
            this.Direction = direction;
        }

        public override void Execute()
        {
            GameObject.Move(Map, Direction);
        }
    }
}
