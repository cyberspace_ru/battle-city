﻿using System;
using System.Collections.Generic;
using BattleCity.Architecture.States;

namespace BattleCity.Architecture
{
    public abstract class Command
    {
        abstract public void Execute();
    }
}
