﻿using System;
using System.Collections.Generic;
using BattleCity.Architecture.Enums;
using BattleCity.Architecture.Configurations;
using UnityEngine;

namespace BattleCity.Architecture.Initializers
{
    class TextureMapInitializer : MapInitializer
    {
        private Texture2D texture;

        public TextureMapInitializer(Texture2D texture)
        {
            this.texture = texture;
        }

        public override Size GetSize()
        {
            return new Size(texture.width, texture.height);
        }

        public override Map Initialize()
        {
            return new Map(GetSize(), DefineGameObjectsByTexture(texture));
        }

        private List<GameObject> DefineGameObjectsByTexture(Texture2D texture)
        {
            List<GameObject> result = new List<GameObject>();
            Color[] pix = texture.GetPixels();
            for (int i = 0; i < texture.width; i++)
            {
                for (int j = 0; j < texture.height; j++)
                {
                    Color color = pix[j * texture.width + i];
                    GameObjectType type = GameObjectType.Undefined;
                    if (color == Color.green)
                    {
                        type = GameObjectType.Player;
                    }
                    else if (color == Color.red)
                    {
                        type = GameObjectType.DoubleTower;
                    }
                    else if (color == Color.magenta)
                    {
                        type = GameObjectType.QuadTower;
                    }
                    else if (color == Color.black)
                    {
                        type = GameObjectType.Brick;
                    }
                    else if (color == Color.cyan)
                    {
                        type = GameObjectType.Concrete;
                    }
                    else if (color == Color.blue)
                    {
                        type = GameObjectType.Water;
                    }
                    if (type != GameObjectType.Undefined)
                    {
                        if (type == GameObjectType.Player && result.Find(x => x.Type == GameObjectType.Player) != null)
                            throw new Exception("Player has been declared more than one time.");
                        GameObject go = factory.Produce(new GameObjectConfiguration
                        {
                            Type = type,
                            Direction = MoveType.Up,
                            Point = new Point(i, (texture.width - 1) - j)
                        });
                        Map map = new Map(GetSize(), result);
                        if (!map.CanBePlaced(go.Collider, go))
                            throw new Exception("Can't place a game object \"" + type.ToString() + "\" at " + go.Collider.Point.ToString() + ".");
                        result.Add(go);
                    }
                }
            }
            return result;
        }

    }
}
