﻿using BattleCity.Architecture.Configurations;

namespace BattleCity.Architecture.Initializers
{
    public interface IInitializerOf<InitializableObject>
    {
        InitializableObject Initialize();
    }
}
