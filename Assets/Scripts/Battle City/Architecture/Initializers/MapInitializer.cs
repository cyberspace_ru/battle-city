﻿using System;
using System.Collections.Generic;
using BattleCity.Architecture.Configurations;
using BattleCity.Architecture.Factories;

namespace BattleCity.Architecture.Initializers
{
    public abstract class MapInitializer : IInitializerOf<Map>
    {
        protected GameObjectFactory factory = new GameObjectFactory();

        abstract public Size GetSize();
        abstract public Map Initialize();
    }
}
