﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BattleCity.Architecture.Enums;

namespace BattleCity.Architecture
{
    public class Bonus : Barrier
    {
        public BonusType BonusType;

        public override void OnTrigger(Map map, GameObject solid)
        {
            if (solid is Player)
                this.Destroy(map);
        }
    }
}
