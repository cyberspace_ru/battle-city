﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BattleCity.Architecture.CollisionSystem;
using BattleCity.Architecture.Agents;

namespace BattleCity.Architecture
{
    public class Missile : DynamicGameObject
    {
        public GameObject Owner;

        public override void Start(Map map)
        {
            base.Start(map);
            // Agents.Add(new MovableAgent(map, this)); // Так тоже можно =) PS: ракеты башен себе урона не наносят.
        }

        public override void Update(Map map)
        {
            Move(map, Direction);
        }

        public override void OnTrigger(Map map, GameObject solid)
        {
            if (!(solid is LiquidBarrier))
                this.Destroy(map);
        }

    }
}
