﻿using System;
using System.Collections.Generic;
using BattleCity.Architecture;
using BattleCity.Architecture.Enums;

namespace BattleCity.Architecture
{
    public class Tower : SolidBarrier
    {
        public TowerBehaviour behaviour;

        public override void Start(Map map)
        {
            if (Random.NextDouble() > 0.5f)
                behaviour = new TriggerShootingBehaviour(this);
            else behaviour = new RandomShootingBehaviour(this);
        }

        public override void Update(Map map)
        {
            behaviour.UpdateByBehaviour(map);
        }
    }
}
