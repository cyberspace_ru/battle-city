﻿using System;
using System.Collections.Generic;
using BattleCity.Architecture.Agents;

namespace BattleCity.Architecture
{
    public class Player : Tank
    { 
        public override void Start(Map map)
        {
            base.Start(map);
            Agents.Add(new MovableAgent(map, this));
            Agents.Add(new ShootableAgent(map, this));
        }

        public override void Update(Map map)
        {
            base.Update(map);
        }
    }
}
