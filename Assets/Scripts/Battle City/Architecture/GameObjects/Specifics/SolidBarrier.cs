﻿using System;
using System.Collections.Generic;
using BattleCity.Architecture.States;

namespace BattleCity.Architecture
{
    public class SolidBarrier : Barrier
    {
        public int Health = 1;
        public bool IgnoreDamage;

        public override void OnCollision(Map map, GameObject trigger)
        {
            if (trigger is Missile)
            {
                Missile missile = (Missile)trigger;
                if (missile.Owner is Player)
                {
                    Player player = (Player)missile.Owner;
                    TankState tank = player.CurrentTankState;
                    Health -= IgnoreDamage && !tank.IgnoreImmortality ? 0 : 1;
                    if (Health <= 0)
                        this.Destroy(map);
                }
            }
        }
    }
}
