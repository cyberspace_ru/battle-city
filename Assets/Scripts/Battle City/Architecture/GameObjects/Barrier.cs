﻿using System;
using System.Collections.Generic;
using BattleCity.Architecture.Enums;

namespace BattleCity.Architecture
{
    public abstract class Barrier : StaticGameObject
    {
        public override void Start(Map map) { }
        public override void OnCollision(Map map, GameObject trigger) { }
        public override void OnTrigger(Map map, GameObject solid) { }
        public override void Update(Map map) { }
    }
}
