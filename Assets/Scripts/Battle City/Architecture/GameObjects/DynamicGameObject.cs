﻿using System;
using System.Collections.Generic;
using BattleCity.Architecture.Enums;
using BattleCity.Architecture.States;
using BattleCity.Architecture.Agents;
using BattleCity.Architecture.Abilities;
using BattleCity.Architecture.CollisionSystem;

namespace BattleCity.Architecture
{
    public abstract class DynamicGameObject : GameObject, IMoveable
    {
        public event Action<MoveType> OnChangeDirection;
        public Point PreviousPoint { get { return position.Previous; } }
        public Point CurrentPoint { get { return position.Current; } }

        private MovingAbility moving;

        public override void Start(Map map)
        {
            this.position = new PositionStateBundle(Collider.Point);
            this.moving = new MovingAbility(UpdateRate - 1);
            this.Updatables.Add(position);
            this.Updatables.Add(moving);
        }

        public override void OnCollision(Map map, GameObject trigger) { }
        public override void OnTrigger(Map map, GameObject solid) { }
        public override void Update(Map map) { }

        public void ChangeDirection(MoveType direction)
        {
            this.Direction = direction;
            if (OnChangeDirection != null)
                OnChangeDirection(direction);
        }

        public void ChangePoint(Collider collider)
        {
            Collider = collider;
            position.ApplyNewState(collider.Point);
        }

        public void Move(Map map, MoveType direction)
        {
            moving.MoveDirection = direction;
            moving.Use(map, this);
        }

    }
}
