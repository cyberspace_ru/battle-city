﻿using System;
using System.Collections.Generic;
using BattleCity.Architecture.Enums;
using BattleCity.Architecture.States;
using BattleCity.Architecture.Agents;
using BattleCity.Architecture.CollisionSystem;

using BattleCity.Architecture.Abilities;
namespace BattleCity.Architecture
{
    public abstract class GameObject
    {
        public bool HasDestroyed { get; private set; }
        public int CurrentUpdateRate { get; private set; }
        public int UpdateRate = 1;
        public Collider Collider;

        protected PositionStateBundle position;

        public GameObjectType Type;
        public MoveType Direction;

        public List<Agent> Agents = new List<Agent>();
        public List<IUpdatable> Updatables = new List<IUpdatable>();

        public event Action OnDestroy;

        abstract public void Start(Map map);
        abstract public void Update(Map map);

        /// <summary>
        /// Возникает когда текущий объект не является триггером, но его коллайдер перекрывает триггер.
        /// </summary>
        abstract public void OnCollision(Map map, GameObject trigger);

        /// <summary>
        /// Возникает когда триггер срабатывает.
        /// </summary>
        abstract public void OnTrigger(Map map, GameObject solid);

        public virtual void OnExitBounds(Map map)
        {
            this.Destroy(map);
        }

        public void Destroy(Map map)
        {
            map.Instances.Remove(this);
            HasDestroyed = true;
            if (OnDestroy != null)
                OnDestroy();
        }

        // Цикл обновления объекта.
        public void ForceUpdate(Map map)
        {
            CurrentUpdateRate++;
            Updatables.ForEach(x => x.BeforeUpdate());
            if (UpdateRate == CurrentUpdateRate)
            {
                CurrentUpdateRate = 0;
                Updatables.ForEach(x => x.OnUpdate());
                // Возбуждаем состояния новыми значениями.
                Agents.ForEach(x => x.ExecuteCommand()); // С помощью внешних воздействий.
                Update(map); // Или с помощью игровой логики.
            }
            Updatables.ForEach(x => x.AfterUpdate());
        }

    }
}
