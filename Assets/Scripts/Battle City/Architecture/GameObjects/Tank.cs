﻿using System;
using System.Collections.Generic;
using BattleCity.Architecture.Enums;
using BattleCity.Architecture.States;
using BattleCity.Architecture.Agents;
using BattleCity.Architecture.Abilities;
using BattleCity.Architecture.CollisionSystem;

namespace BattleCity.Architecture
{
    public abstract class Tank : DynamicGameObject, IShootable
    {
        public TankState CurrentTankState { get { return bundle.Current; } }
        public TankState PreviousTankState { get { return bundle.Previous; } }
        public TankState DefaultTankState { get { return bundle.Default; } }

        private int health = 10;
        private TankStateBundle bundle = new TankStateBundle(TankState.Default);
        private ShootingAbility shooting;

        public override void OnCollision(Map map, GameObject trigger)
        {
            if (trigger is Missile && !CurrentTankState.IsImmortality)
            {
                // При попадании ракеты.
                UnityEngine.Debug.Log("Damage consumed.");
                if ((health -= 1) <= 0)
                    this.Destroy(map);
            }
            else if (trigger is Bonus)
            {
                // При наезде на бонус.
                Bonus bonus = (Bonus)trigger;
                bundle.ToDefault();
                switch (bonus.BonusType)
                {
                    case BonusType.Attack:
                        bundle.ApplyNewState(TankState.AttackBonus);
                        break;
                    case BonusType.Clock:
                        bundle.ApplyNewState(TankState.ClockBonus);
                        break;
                    case BonusType.Defence:
                        bundle.ApplyNewState(TankState.DefenceBonus);
                        break;
                }
                bundle.SetDefaultIn(100);
            }
        }

        public override void Start(Map map)
        {
            base.Start(map);
            this.shooting = new ShootingAbility(CurrentTankState.ShootDelay);
            // Для обновления кулдауна стрельбы.
            this.Updatables.Add(shooting);
            // Для обновления таймера в случаи когда мы поднимаем бонус.
            this.Updatables.Add(bundle);
            // Подписываемся на изменения текущекго состояния танка.
            bundle.OnChangeCurrentValue += (x) => shooting.Delay = x.ShootDelay;
        }

        public void Shoot(Map map)
        {
            shooting.Use(map, this);
        }

    }
}