﻿using System;
using System.Collections.Generic;
using BattleCity.Architecture.Enums;
using BattleCity.Architecture.CollisionSystem;
using BattleCity.Architecture.Abilities;

namespace BattleCity.Architecture
{
    class TriggerShootingBehaviour : TowerBehaviour
    {
        private int radius;

        public TriggerShootingBehaviour(Tower tower) : base(tower, 5)
        {
            radius = 15 + Random.Next(10);
        }

        public override void UpdateByBehaviour(Map map)
        {
            MoveTypeExtension.AsList().ForEach(x =>
            {
                // Если башня с двумя пушками, то проверяем, что предлженное направление совпадает с ориентацией (пространственной!!!) башни.
                if (tower.Type == GameObjectType.DoubleTower && x.ToOrientationType() != tower.Direction.ToOrientationType())
                    return;
                if (HasPlayerOnDirection(map, x))
                {
                    tower.Direction = x;
                    shooting.Use(map, tower);
                }
            });
        }

        private bool HasPlayerOnDirection(Map map, MoveType direction)
        {
            List<GameObject> collisions = RayCast.CastBySolidObjects(map, tower, direction, radius);
            if (collisions != null)
                return collisions.Find(x => x is Player) != null;
            return false;
        }
    }
}
