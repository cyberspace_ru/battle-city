﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BattleCity.Architecture.Abilities;

namespace BattleCity.Architecture
{
    public class RandomShootingBehaviour : TowerBehaviour
    {
        public RandomShootingBehaviour(Tower tower) : base(tower, 1) { }

        public override void UpdateByBehaviour(Map map)
        {
            if (Random.NextDouble() > 0.9)
                shooting.Use(map, tower);
        }
    }
}
