﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BattleCity.Architecture.Abilities;

namespace BattleCity.Architecture
{
    public abstract class TowerBehaviour
    {
        protected Tower tower;
        protected ShootingAbility shooting;

        public TowerBehaviour(Tower tower, int shootDelay)
        {
            this.tower = tower;
            this.shooting = new ShootingAbility(shootDelay);
            this.tower.Updatables.Add(shooting);
        }

        abstract public void UpdateByBehaviour(Map map);
    }
}
