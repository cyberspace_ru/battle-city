﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleCity.Architecture.States
{
    public abstract class StateBundle<T> : IUpdatable
    {
        public T Previous;
        public T Current
        {
            get
            {
                return _current;
            }
            set
            {
                _current = value;
                if (OnChangeCurrentValue != null)
                    OnChangeCurrentValue(value);
            }
        }
        public readonly T Default;
        public event Action<T> OnChangeCurrentValue;

        private T _current;

        public StateBundle(T defaultState)
        {
            Default = Previous = Current = defaultState;
        }

        public void ToDefault()
        {
            Previous = Current = Default;
        }

        public void ApplyNewState(T state)
        {
            Previous = Current;
            Current = state;
        }

        public abstract void BeforeUpdate();
        public abstract void OnUpdate();
        public abstract void AfterUpdate();
    }
}
