﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleCity.Architecture.States
{
    public class PositionStateBundle : StateBundle<Point>
    {
        public PositionStateBundle(Point defaultState) : base(defaultState) { }

        public override void AfterUpdate()
        {
        }

        public override void BeforeUpdate()
        {
        }

        public override void OnUpdate()
        {
            Previous = Current;
        }
    }
}
