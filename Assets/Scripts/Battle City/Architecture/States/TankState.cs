﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleCity.Architecture.States
{
    public struct TankState
    {
        public static TankState Default { get { return new TankState(false, false, 5); } }
        public static TankState AttackBonus { get { return new TankState(false, true, 5); } }
        public static TankState ClockBonus { get { return new TankState(false, false, 1); } }
        public static TankState DefenceBonus { get { return new TankState(true, false, 5); } }

        public bool IsImmortality { get; private set; }
        public bool IgnoreImmortality { get; private set; }
        public int ShootDelay { get; private set; }

        public TankState(bool isImmortality, bool ignoreImmortality, int shootDelay)
        {
            this.IsImmortality = isImmortality;
            this.IgnoreImmortality = ignoreImmortality;
            this.ShootDelay = shootDelay;
        }
    }
}
