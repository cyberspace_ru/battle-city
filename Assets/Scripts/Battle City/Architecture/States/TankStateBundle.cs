﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleCity.Architecture.States
{
    public class TankStateBundle : StateBundle<TankState>
    {
        private int time;
        public TankStateBundle(TankState state) : base(state) { }

        public void SetDefaultIn(int time)
        {
            this.time = time;
        }

        public override void BeforeUpdate()
        {
        }

        public override void OnUpdate()
        {
            if (time-- <= 0)
                this.ToDefault();
        }

        public override void AfterUpdate()
        {
        }
    }
}
