﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImmortalEffect : MonoBehaviour
{
    public Color EmissionFrom;
    public Color EmissionTo;
    public AnimationCurve Curve;

    private Material mat;
    private Color defaultEmission;
    private float current;

    void Start()
    {
        mat = GetComponent<MeshRenderer>().material;
        defaultEmission = mat.GetColor("_EmissionColor");
    }

    void Update()
    {
        if (mat == null)
            return;
        current += Time.deltaTime;
        if (current > 1)
            current = 0;
        mat.SetColor("_EmissionColor", Color.Lerp(EmissionFrom, EmissionTo, Curve.Evaluate(current)));
    }

    void OnEnable()
    {
        current = 0;
    }

    void OnDisable()
    {
        if (mat != null)
            mat.SetColor("_EmissionColor", defaultEmission);
    }
}
