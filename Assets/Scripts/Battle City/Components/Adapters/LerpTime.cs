﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LerpTime : MonoBehaviour
{
    [Range(1, 60)]
    public int GameSpeed = 1;
    public CoreAdapter Core;
    public NextTurnEvent OnNextTurn;

    private int current;

    public void Reset()
    {
        Application.targetFrameRate = 60;
        current = 0;
    }

    void Update()
    {
        if (!Core.Paused && Core.Adaptable != null && !Core.Adaptable.Finished)
        {
            if (current++ >= GameSpeed)
                NextTurn();
        }
    }

    public float GetLerpRatio(BattleCity.Architecture.GameObject gameObject)
    {
        float reached = GameSpeed * gameObject.CurrentUpdateRate + current;
        float requered = GameSpeed * gameObject.UpdateRate;
        return reached / requered;
    }

    void NextTurn()
    {
        current = 0;
        OnNextTurn.Invoke();
    }

    [System.Serializable]
    public class NextTurnEvent : UnityEngine.Events.UnityEvent
    {

    }

}
