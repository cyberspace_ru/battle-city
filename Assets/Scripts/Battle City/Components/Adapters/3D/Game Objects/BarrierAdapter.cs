﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BattleCity.Architecture;
using BattleCity.Architecture.Enums;

public class BarrierAdapter : StaticGameObjectAdapter
{
    public void Adapte(Barrier adaptable)
    {
        base.Adapte(adaptable);
        if (adaptable is Tower)
        {
            Tower tower = (Tower)adaptable;
            if (tower.Type == GameObjectType.DoubleTower && tower.Direction.ToOrientationType() == OrientationType.Horizontal)
                transform.GetChild(0).Rotate(new Vector3(0, 1, 0), 90);
        }
    }

    public static void CreateBarrier(Barrier barrier)
    {
        UnityEngine.GameObject go = Instantiate(LoadPrefab("Barriers/" + barrier.Type.ToString()));
        go.GetComponent<BarrierAdapter>().Adapte(barrier);
    }
}
