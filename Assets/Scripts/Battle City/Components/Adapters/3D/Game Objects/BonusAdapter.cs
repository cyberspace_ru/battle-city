﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BattleCity.Architecture;
using BattleCity.Architecture.Enums;

public class BonusAdapter : GameObjectAdapter<Bonus>
{
    public new void Adapte(Bonus adaptable)
    {
        base.Adapte(adaptable);
    }

    public static void CreateBonus(Bonus bonus)
    {
        UnityEngine.GameObject go = Instantiate(LoadPrefab("Bonuses/" + bonus.BonusType.ToString()));
        go.GetComponent<BonusAdapter>().Adapte(bonus);
    }

}
