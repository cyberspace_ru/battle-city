﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BattleCity.Architecture;
using BattleCity.Architecture.States;

public class PlayerAdapter : DynamicGameObjectAdapter
{
    public ImmortalEffect DefenceEffect;
    public UnityEngine.GameObject AtackEffect;
    public UnityEngine.GameObject ClockEffect;

    public void Adapte(Player adaptable)
    {
        base.Adapte(adaptable);
    }

    public static void CreatePlayer(Player player)
    {
        UnityEngine.GameObject go = Instantiate(LoadPrefab("Player"));
        go.GetComponent<PlayerAdapter>().Adapte(player);
    }

    private new void Update()
    {
        base.Update();
        Player player = (Player)Adaptable;
        TankState current = player.CurrentTankState;
        DefenceEffect.enabled = current.IsImmortality;
        AtackEffect.SetActive(current.IgnoreImmortality);
        ClockEffect.SetActive(current.ShootDelay < player.DefaultTankState.ShootDelay);
    }
}
