﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BattleCity.Architecture.Enums;
using BattleCity.Architecture;

public class DynamicGameObjectAdapter : GameObjectAdapter<DynamicGameObject>
{
    public new void Adapte(DynamicGameObject adaptable)
    {
        base.Adapte(adaptable);
        Adaptable.OnChangeDirection += OnChangePointHandler;
    }

    public void OnChangePointHandler(MoveType direction)
    {
        Transform toRotate = transform.GetChild(0);
        switch (direction)
        {
            case MoveType.Up:
                toRotate.rotation = Quaternion.Euler(0, 0, 0);
                break;
            case MoveType.Right:
                toRotate.rotation = Quaternion.Euler(0, 90, 0);
                break;
            case MoveType.Down:
                toRotate.rotation = Quaternion.Euler(0, 180, 0);
                break;
            default:
                toRotate.rotation = Quaternion.Euler(0, 270, 0);
                break;
        }
    }

    public void Update()
    {
        Vector3 lerpPositionAsFrom = GetPositionByPoint(Adaptable.PreviousPoint);
        Vector3 lerpPositionAsTo = GetPositionByPoint(Adaptable.CurrentPoint);
        float t = timer.GetLerpRatio(Adaptable);
        transform.position = Vector3.Lerp(lerpPositionAsFrom, lerpPositionAsTo, t);
    }
}
