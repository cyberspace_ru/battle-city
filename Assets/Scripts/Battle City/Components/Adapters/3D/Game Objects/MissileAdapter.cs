﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BattleCity.Architecture;
using BattleCity.Architecture.Enums;

public class MissileAdapter : DynamicGameObjectAdapter
{
    public void Adapte(Missile adaptable)
    {
        base.Adapte(adaptable);
        if (adaptable.Direction == MoveType.Down || adaptable.Direction == MoveType.Right)
            transform.GetChild(0).transform.Rotate(new Vector3(0, 1, 0), 180);
    }

    public static void CreateMissle(Missile missile)
    {
        string ownerName = missile.Owner.Type.ToString();
        string spreadTypeName = missile.Direction.ToOrientationType().ToString();
        UnityEngine.GameObject go = Instantiate(LoadPrefab("Missiles/" + ownerName + "-" + spreadTypeName));
        go.GetComponent<MissileAdapter>().Adapte(missile);
    }

}
