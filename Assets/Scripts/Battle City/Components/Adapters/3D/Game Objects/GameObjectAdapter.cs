﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BattleCity.Architecture.Agents;

public class GameObjectAdapter<T> : Adapter<T> where T : BattleCity.Architecture.GameObject
{
    public BaseGameObjectEvent OnDestroy;
    private MapAdapter mapAdapter;
    protected LerpTime timer;

    public new void Adapte(T adaptable)
    {
        base.Adapte(adaptable);

        AddAgents(adaptable, adaptable.Agents);

        mapAdapter = GameObject.FindGameObjectWithTag("Map").GetComponent<MapAdapter>();
        timer = GameObject.FindGameObjectWithTag("Time").GetComponent<LerpTime>();

        transform.position = mapAdapter.DefinePosition(adaptable);
        transform.parent = mapAdapter.transform;

        adaptable.OnDestroy += () => {
            if (OnDestroy != null)
            {
                OnDestroy.Invoke();
            }
            Destroy(gameObject);
        };
    }

    protected Vector3 GetPositionByPoint(Point point)
    {
        return mapAdapter.DefinePosition(point);
    }

    private void AddAgents(BattleCity.Architecture.GameObject driven, List<Agent> agents)
    {
        agents.ForEach(x =>
        {
            if (x is MovableAgent)
            {
                // if (driven is BattleCity.Architecture.Player) // Открывает возможность управления ракетами контроллером игрока.
                    gameObject.AddComponent<PlayerMovableAgent>().Adapte((MovableAgent)x);
            }
            else if (x is ShootableAgent)
            {
                if (driven is BattleCity.Architecture.Player)
                    gameObject.AddComponent<PlayerShootableAgent>().Adapte((ShootableAgent)x);
            }
        });
    }

    protected static GameObject LoadPrefab(string path)
    {
        return Resources.Load(path) as GameObject;
    }

    private void OnDrawGizmos()
    {
        if (Adaptable == null)
            return;
        Gizmos.color = Color.green;
        Vector3 position = transform.position;
        float size = Adaptable.Collider.Size.Width * 0.25f;
        Vector3 cubeSize = new Vector3(size, 1, size);
        Gizmos.DrawCube(position + new Vector3(size / 2, 0, -size / 2), cubeSize);
    }

    [System.Serializable]
    public class BaseGameObjectEvent : UnityEngine.Events.UnityEvent
    {

    }

}
