﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BattleCity.Architecture;

public class StaticGameObjectAdapter : GameObjectAdapter<StaticGameObject>
{
    public new void Adapte(StaticGameObject adaptable)
    {
        base.Adapte(adaptable);
    }
}
