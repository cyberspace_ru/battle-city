﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BattleCity.Architecture.Agents;

public class PlayerShootableAgent : Adapter<ShootableAgent>
{
    public new void Adapte(ShootableAgent adaptable)
    {
        base.Adapte(adaptable);
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Space) || Input.GetKey(KeyCode.E))
        {
            Adaptable.Shoot();
        }
    }
}
