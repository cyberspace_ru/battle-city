﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BattleCity.Architecture.Agents;

public class PlayerMovableAgent : Adapter<MovableAgent>
{
    public new void Adapte(MovableAgent adaptable)
    {
        base.Adapte(adaptable);
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.W))
        {
            Adaptable.MoveToUp();
        }
        else if (Input.GetKey(KeyCode.D))
        {
            Adaptable.MoveToRight();
        }
        else if (Input.GetKey(KeyCode.S))
        {
            Adaptable.MoveToDown();
        }
        else if (Input.GetKey(KeyCode.A))
        {
            Adaptable.MoveToLeft();
        }
    }
}
