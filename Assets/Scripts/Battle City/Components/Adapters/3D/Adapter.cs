﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Adapter<AdaptableObject> : MonoBehaviour
{
    public AdaptableObject Adaptable { get; set; }

    public void Adapte(AdaptableObject adaptable)
    {
        this.Adaptable = adaptable;
    }
}
