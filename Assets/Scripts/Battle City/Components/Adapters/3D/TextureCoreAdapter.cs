﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BattleCity.Architecture.Initializers;

public class TextureCoreAdapter : CoreAdapter
{
    public Texture2D Texture;

    public void Start()
    {
        Texture = GameController.Instance.CurrentMap;
        StartGame();
    }

    public void StartGame()
    {
        if (Texture != null)
            StartGame(new TextureMapInitializer(Texture));
    }
}