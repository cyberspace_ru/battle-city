﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BattleCity.Architecture;

[RequireComponent(typeof(MeshRenderer))]
public class MapAdapter : Adapter<Map>
{
    private Vector2[,] field;

    public new void Adapte(Map adaptable)
    {
        base.Adapte(adaptable);
        MeshRenderer meshRenderer = GetComponent<MeshRenderer>();
        field = CreateField(adaptable.Size, meshRenderer.bounds);
        adaptable.Instances.ForEach(x => SetGameObject(x));
        adaptable.OnCreateInstance += SetGameObject;
    }

    public Vector3 DefinePosition(BattleCity.Architecture.GameObject go)
    {
        Point point = go.Collider.Point;
        return DefinePosition(point);
    }

    public Vector3 DefinePosition(Point point)
    {
        Vector2 cell = field[point.X, point.Y];
        return new Vector3(cell.x, 0, cell.y);
    }

    private void SetGameObject(BattleCity.Architecture.GameObject go)
    {
        if (go is Bonus)
            BonusAdapter.CreateBonus((Bonus)go);
        else if (go is Barrier)
            BarrierAdapter.CreateBarrier((Barrier)go);
        else if (go is Player)
            PlayerAdapter.CreatePlayer((Player)go);
        else if (go is Missile)
            MissileAdapter.CreateMissle((Missile)go);
    }

    private static Vector2[,] CreateField(Size fieldSize, Bounds meshBounds)
    {
        Vector2[,] field = new Vector2[fieldSize.Width, fieldSize.Height];
        Vector2 cellSize = new Vector2(meshBounds.size.x / fieldSize.Width, meshBounds.size.z / fieldSize.Height);
        Vector2 defCursor = new Vector2(meshBounds.min.x, meshBounds.max.z);
        for (int i = 0; i < field.GetLength(0); i++)
            for (int j = 0; j < field.GetLength(1); j++)
                field[i, j] = defCursor + new Vector2(cellSize.x * i, -cellSize.y * j);
        return field;
    }

    private void OnDrawGizmos()
    {
        field.ToMaybe().Do(x =>
        {
            float y = transform.position.y;
            Gizmos.color = Color.red;
            for (int i = 0; i < x.GetLength(0); i++)
            {
                for (int j = 0; j < x.GetLength(1); j++)
                {
                    Vector2 cell = x[i, j];
                    Vector3 worldPosition = new Vector3(cell.x, y, cell.y);
                    Gizmos.DrawLine(worldPosition, worldPosition + Vector3.up);
                }
            }
        });
    }
}
