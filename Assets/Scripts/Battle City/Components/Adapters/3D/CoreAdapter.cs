﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BattleCity.Architecture;
using BattleCity.Architecture.Initializers;

public class CoreAdapter : Adapter<Core>
{
    public MapAdapter MapAdapter;
    public UnityEngine.GameObject Menu;
    public LerpTime Timer;
    public BaseCoreEvent OnWin;
    public BaseCoreEvent OnDefeat;

    public bool Paused { get; private set; }

    protected void StartGame(MapInitializer initializer)
    {
        Reset();
        Core core = new Core();
        ConstrainScene(initializer.GetSize());
        core.Launch(initializer);
        core.OnWin += OnWin.Invoke;
        core.OnDefeat += OnDefeat.Invoke;
        Adapte(core);
        Paused = false;
    }

    protected void Reset()
    {
        Adaptable = null;
        Timer.Reset();
        for (var i = MapAdapter.transform.childCount - 1; i >= 0; i--)
        {
            Transform child = MapAdapter.transform.GetChild(i);
            child.transform.parent = null;
            Destroy(child.gameObject);
        }
    }

    public void Back()
    {
        GameController.Instance.ToMainMenu();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
            Paused = !Paused;
        if (Paused || this.Adaptable == null || this.Adaptable.Finished)
        {
            Menu.SetActive(true);
        }
        else
        {
            Menu.SetActive(false);
        }
    }

    public void ConstrainScene(Size size)
    {
        MapAdapter.transform.localScale = new Vector3(size.Width / 40.0f, 1, size.Height / 40.0f);
        Camera.main.transform.position = new Vector3(0, (size.Width / 40.0f) * 9, (size.Width / 40.0f) * -2);
    }

    public void SayAboutWin()
    {
        Debug.Log("You Won!");
    }

    public void SayAboutDefeat()
    {
        Debug.Log("You lose!");
    }

    /// <summary>
    /// Старт адаптации.
    /// </summary>
    public new void Adapte(Core adaptable)
    {
        base.Adapte(adaptable);
        MapAdapter.Adapte(adaptable.Map);
    }

    public void NextTurn()
    {
        Adaptable.ToMaybe().Do(x => x.ForceUpdate());
    }

    [System.Serializable]
    public class BaseCoreEvent : UnityEngine.Events.UnityEvent
    {

    }

}

