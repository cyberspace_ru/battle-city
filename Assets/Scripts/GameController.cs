﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public static GameController Instance { get; private set; }
    public Texture2D CurrentMap;

    void Awake()
    {
        if (Instance != null)
            Destroy(this.gameObject);
        else
        {
            Instance = this;
            DontDestroyOnLoad(transform.gameObject);
        }
    }

    public void LaunchGame()
    {
        GameObject core = GameObject.FindGameObjectWithTag("Core");
        if (core != null)
            core.GetComponent<TextureCoreAdapter>().StartGame();
        else if (CurrentMap != null)
            SceneManager.LoadScene("Game Scene", LoadSceneMode.Single);
    }

    public void ToMainMenu()
    {
        SceneManager.LoadScene("Map Menu", LoadSceneMode.Single);
    }

}
