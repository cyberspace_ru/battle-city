﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TransformExtension
{
    public static void DeleteAllChilds(this Transform transform)
    {
        int childs = transform.childCount;
        for (int i = childs - 1; i >= 0; i--)
            GameObject.DestroyImmediate(transform.GetChild(i).gameObject);
    }

}
