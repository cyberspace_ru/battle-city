﻿using System;
using UnityEngine;

// https://lostechies.com/derickbailey/2010/09/30/monads-in-c-which-part-is-the-monad/
public static class MaybeExtensions
{
    public static Maybe<T> ToMaybe<T>(this T input)
    {
        return new Maybe<T>(input);
    }

    public static Maybe<TResult> Get<TInput, TResult>(this Maybe<TInput> maybe, Func<TInput, TResult> func)
    {
        Maybe<TResult> result;
        if (maybe.HasValue)
            result = new Maybe<TResult>(func(maybe.Value));
        else
            result = Maybe<TResult>.None;
        return result;
    }

    public static Maybe<TInput> Do<TInput>(this Maybe<TInput> maybe, Action<TInput> action)
    {
        Maybe<TInput> result = maybe;
        if (maybe.HasValue) action(maybe.Value);
        else result = Maybe<TInput>.None;
        return result;
    }

    public static Maybe<TInput> Do<TInput>(this Maybe<TInput> maybe, Action action)
    {
        Maybe<TInput> result = maybe;
        if (maybe.HasValue) action();
        else result = Maybe<TInput>.None;
        return result;
    }

    public static Maybe<TInput> Failed<TInput>(this Maybe<TInput> maybe, Action<TInput> action)
    {
        Maybe<TInput> result = maybe;
        if (!maybe.HasValue) action(maybe.Value);
        else result = maybe;
        return result;
    }

    public static Maybe<TInput> Failed<TInput>(this Maybe<TInput> maybe, Action action)
    {
        Maybe<TInput> result = maybe;
        if (!maybe.HasValue) action();
        else result = maybe;
        return result;
    }

    public static Maybe<TInput> If<TInput>(this Maybe<TInput> maybe, bool statement)
    {
        Maybe<TInput> result;
        if (maybe.HasValue && statement)
            result = maybe;
        else
            result = Maybe<TInput>.None;
        return result;
    }

    public static Maybe<TInput> If<TInput>(this Maybe<TInput> maybe, Func<bool> func)
    {
        Maybe<TInput> result;
        if (maybe.HasValue && func())
            result = maybe;
        else
            result = Maybe<TInput>.None;
        return result;
    }

    public static Maybe<TInput> If<TInput>(this Maybe<TInput> maybe, Func<TInput, bool> func)
    {
        Maybe<TInput> result;
        if (maybe.HasValue && func(maybe.Value))
            result = maybe;
        else
            result = Maybe<TInput>.None;
        return result;
    }

    public static TInput Return<TInput>(this Maybe<TInput> maybe)
    {
        return maybe.Value;
    }

    public static TResult Return<TInput, TResult>(this Maybe<TInput> maybe, Func<TInput, TResult> func, TResult defaultValue)
    {
        TResult result = defaultValue;
        if (maybe.HasValue)
            result = func(maybe.Value);
        return result;
    }

    public static TInput Return<TInput>(this Maybe<TInput> maybe, TInput defaultValue)
    {
        TInput result = defaultValue;
        if (maybe.HasValue)
            result = maybe.Value;
        return result;
    }

    public static Maybe<TInput> Warning<TInput>(this Maybe<TInput> maybe, string warnMessage)
    {
        Maybe<TInput> result = maybe;
        if (!maybe.HasValue)
            Debug.LogWarning(warnMessage);
        return result;
    }

    public static Maybe<TInput> Error<TInput>(this Maybe<TInput> maybe, string warnMessage)
    {
        Maybe<TInput> result = maybe;
        if (!maybe.HasValue)
            Debug.LogError(warnMessage);
        return result;
    }
}