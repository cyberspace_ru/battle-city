﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TransformListExtension
{
    public static Transform FindNearest(this List<Transform> transforms, Transform source)
    {
        Transform bestTarget = null;
        float closestDistanceSqr = Mathf.Infinity;
        Vector3 currentPosition = source.position;
        foreach (Transform potentialTarget in transforms)
        {
            float dSqrToTarget = (potentialTarget.position - currentPosition).sqrMagnitude;
            if (dSqrToTarget < closestDistanceSqr)
            {
                closestDistanceSqr = dSqrToTarget;
                bestTarget = potentialTarget;
            }
        }
        return bestTarget;
    }
}
