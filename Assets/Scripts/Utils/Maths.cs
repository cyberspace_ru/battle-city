﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Maths
{

    private static float PI_DIVIDED_BY_180 = Mathf.PI / 180;

    public static Rect CreateRect(Vector2 position, Vector2 size)
    {
        return new Rect(position, size);
    }

    public static Vector2 GetPointOnCircle(Vector2 center, float radius, float angle)
    {
        float temp = angle * PI_DIVIDED_BY_180;
        return new Vector2(center.x + radius * Mathf.Cos(temp), center.y + radius * Mathf.Sin(temp));
    }

}
